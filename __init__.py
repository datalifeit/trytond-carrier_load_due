# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import order


def register():
    Pool.register(
        order.ShipmentOut,
        order.LoadOrder,
        order.Sale,
        order.DueLoadOrderData,
        module='carrier_load_due', type_='model')
    Pool.register(
        order.DoLoadOrder,
        module='carrier_load_due', type_='wizard')
